package pl.sdacademy.usergenerator.main;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	
	private User u;
	private String path = "src\\pl\\sdacademy\\usergenerator\\resources\\";
	private LinkedHashMap<String, LinkedList<String>> hm = new LinkedHashMap<>();
	// konstruktor bezparametrywy
	
	public UserGenerator() { }
	
	// konstruktor z p�ci�
	
	public UserGenerator(UserSex sex) {
		this.u.setSex(sex);
	}
	
	// generowanie daty urodzenia 
	public String generateBirthDate() {
		// w zaleznosci od miesiaca, losujemy dzien
		// nastepnie sprawdzamy przestepnosc roku
		return "";
	}
	
	// generowanie peselu powinno zwracac Exeption jesli birthDate lub sex s� null'em
	// w ramach cwiczen mozna stworzyc nowy wyjatek (wlasny), np. InvalidPESELException
	
	public String generatePESEL() {
		// zabezpieczenie przed generowaniem kiedy data urodzenia jest pusta
		// zabezpieczenie przed generowaniem kiedy pole sex jest puste lub niezdefiniowane
		return "";
	}
	
	// name
	
	public String generateName() {
		// wykorzystujemy metode getFileContent();
		return "";
	}

	// lastname
	public String generateLastname() {
		// wykorzystujemy metode getFileContent();
		return "";
	}
	
	// street
	public String generateStreet() {
		
		return "";
	}
	// city
	public String generateCity() {
		
		return "";
	}
	
	// homeNumber
	public String generateHomeNumber() {
		
		return "";
	}
	
	// zip code
	
	public String generateZipCode() {
		
		return "";
	}
	
	// cCn
	public String generateCCN() {
		
		return "";
	}
	
	// phone
	
	public String generatePhone() {
		
		return "";
	}
	
	// warto napisac dodatkowa metode, ktora odczyta nam plik tekstowy, 
	// a kazda jego linijke doda do np. listy i ja zwroci
	
	public LinkedList<String> getFileContent(String filename) throws Exception {
		if( this.hm.get(filename) == null ) {
			this.hm.put(filename, new LinkedList<String>());
			Scanner s = new Scanner(new File(this.path + filename));
			while(s.hasNextLine()) {
				this.hm.get(filename).add(s.nextLine());
			}
			s.close();
		}
		return this.hm.get(filename);
	}
	
	// metoda prywatna, zwracajaca losowa wartosci z zakresu <from, to>
	private int getRandomNumber(int from, int to) {
		Random r = new Random();
		return r.nextInt( to - from ) + from;
	}
	
	
}
