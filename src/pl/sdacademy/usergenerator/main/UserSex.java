package pl.sdacademy.usergenerator.main;

public enum UserSex {
	SEX_F(1, "Kobieta"), 
	SEX_M(2, "M�czyzna"), 
	SEX_UNDEFINED(3, "Nie okre�lono");
	
	private int id;
	private String sex;
	
	UserSex(int id, String sex) {
		this.id = id;
		this.sex = sex;
	}
	
	public String getSex() {
		return this.sex;
	}
	
	public int getId() {
		return this.id;
	}
}
